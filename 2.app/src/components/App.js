import React, { Component } from 'react';

import Header from './Header';
import Item from './Item';
import Cart from './Cart';

class App extends Component {
  render() {
    return (
      <div className="main-app">
        <div className="menu">
          <Header />
        </div>
        <Item />
        <Cart />
      </div>
    );
  }
}

export default App;
