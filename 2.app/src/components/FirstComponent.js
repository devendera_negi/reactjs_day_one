import React from 'react';

class FirstComponent extends React.Component {
    render() {
        return (
            <p>Hello, First Component</p>
        );
    }
}

export default FirstComponent;