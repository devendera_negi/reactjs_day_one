import React, { Component } from 'react';

class Stuff extends Component {
  render() {
    const { details, index } = this.props;
    const isAvailable = details.status === 'available';
    const buttonText = isAvailable ? 'Add to cart' : 'Sold Out!';
    return (
     <li className="menu-stuff">
        <img src={details.image} alt={details.name} />
        <h3 className="fish-name">
            {details.name}
            <span className="price">{details.price}</span>            
        </h3>
        <p>{details.desc}</p>
        <button disabled={!isAvailable} onClick={() => this.props.addToCart(index) } >{buttonText}</button>
     </li>
    );
  }
}

export default Stuff;