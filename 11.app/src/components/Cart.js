import React, { Component } from 'react';

class Cart extends Component {
  constructor() {
    super();
    this.renderOrder = this.renderOrder.bind(this);
  }
  renderOrder(key) {
    const item = this.props.items[key];
    const count = this.props.cart[key];

    if(!item || item.status === 'unavailable') {
      return <li key={key}> Sorry, {item ? item.name : 'Item'} is not available!</li>
    }

    return (
      <li key={key}>
        <span>{count} {item.name} </span>
        <span className="price">{count * item.price} </span>
      </li>
    )
  }

  render() {
    const cartItemId = Object.keys(this.props.cart);
    const total = cartItemId.reduce((prevTotal, key) => {
      const item = this.props.items[key];
      const count = this.props.cart[key];
      const isAvailable = item && item.status === 'available';
      if(isAvailable) {
        return prevTotal + (count * item.price || 0)
      }
      return prevTotal;
    }, 0);
    return (
      <div className="order-wrap">
        <h2>{cartItemId.map(this.renderOrder)}</h2>
        {total}
      </div>
    );
  }
}

export default Cart;