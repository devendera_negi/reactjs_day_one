module.exports = {
    item1: {
      name: 'Buy Fish',
      image: 'http://i.istockimg.com/file_thumbview_approve/36248396/5/stock-photo-36248396-blackened-cajun-sea-bass.jpg',
      desc: 'Everyones favorite white fish. We will cut it to the size you need and ship it.',
      price: 1724,
      status: 'available'
    },
    item11: {
      name: 'milk',
      image: 'http://i.istockimg.com/file_thumbview_approve/36248396/5/stock-photo-36248396-blackened-cajun-sea-bass.jpg',
      desc: 'Everyones favorite white fish. We will cut it to the size you need and ship it.',
      price: 1724,
      status: 'available'
    },  
    item2: {
      name: 'Buy Lobster',
      image: 'http://i.istockimg.com/file_thumbview_approve/32135274/5/stock-photo-32135274-cooked-lobster.jpg',
      desc: 'These tender, mouth-watering beauties are a fantastic hit at any dinner party.',
      price: 3200,
      status: 'unavailable'
    }    
  };