import Rebase from 're-base';

// var Rebase = require('re-base');
import firebase from 'firebase';
const app = firebase.initializeApp({
    apiKey: "AIzaSyD_j3NaiRMZDIWCeEFnXGnF87vvYrHKr3k",
    authDomain: "shopping-list-cd8e2.firebaseapp.com",
    databaseURL: "https://shopping-list-cd8e2.firebaseio.com",
    projectId: "shopping-list-cd8e2",
    storageBucket: "shopping-list-cd8e2.appspot.com",
    messagingSenderId: "789536231951"
});

const base = Rebase.createClass(app.database());
// const base = Rebase.createClass({
//     apiKey: "AIzaSyD_j3NaiRMZDIWCeEFnXGnF87vvYrHKr3k",
//     authDomain: "shopping-list-cd8e2.firebaseapp.com",
//     databaseURL: "https://shopping-list-cd8e2.firebaseio.com"
// });

export default base;