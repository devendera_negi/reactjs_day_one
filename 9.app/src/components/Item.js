import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ItemForm from './ItemForm';

class Item extends Component {
  render() {
    return (
      <div>
        <ItemForm addItem={this.props.addItem}/>
        <button onClick={this.props.loadDefault}>Load Default</button>
      </div>
    );
  }
}

Item.contextTypes = {
  router: PropTypes.object
}

export default Item;