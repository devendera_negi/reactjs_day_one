import React from 'react';

const Header = (props) => {
  return (
    <header className="head">
      <h1>Shopping list</h1>
      <h3 className="list-name">{props.listItem}</h3>
    </header>
  );
}
export default Header;