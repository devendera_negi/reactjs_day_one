import React, { Component } from 'react';

class ItemForm extends Component {
    createItem(event) {
        event.preventDefault();
        console.log('create item');
        const item = {
            name: this.name.value,
            price: this.price.value,
            status: this.status.value,
            desc: this.desc.value,
            image: this.image.value
        }
        console.log('item:-', item);
        this.props.addItem(item);
        this.itemForm.reset();
    }
    render() {
        return (
            <div>
            <form ref={(input) => this.itemForm = input } className="item-edit" onSubmit={(e) => {this.createItem(e)}}>
            <input ref={(input) => this.name = input} type="text" placeholder="Item Name" />
            <input ref={(input) => this.price = input} type="text" placeholder="Item Price" />
            <select ref={(input) => this.status = input}>
                <option value="found">Found</option>
                <option value="notfound">Not Found</option>
            </select>
            <textarea ref={(input) => this.desc = input} placeholder="Item Desc"></textarea>
            <input ref={(input) => this.image = input} type="text" placeholder="Item image URL" />
            <button type="submit">Add Item</button>
            </form>
            </div>
        );
    }
}

export default ItemForm;