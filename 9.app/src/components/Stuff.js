import React, { Component } from 'react';

class Stuff extends Component {
  render() {
    const { details } = this.props;
    return (
     <li className="menu-stuff">
        <img src={details.image} alt={details.name} />
        <h3 className="fish-name">
            {details.name}
            <span className="price">{details.price}</span>            
        </h3>
        <p>{details.desc}</p>
        <button>Add to Cart</button>
     </li>
    );
  }
}

export default Stuff;