import React from 'react';
// import ReactDOM from 'react-dom';
import { render } from 'react-dom';

import registerServiceWorker from './registerServiceWorker';

class FirstComponent extends React.Component {
    render() {
        return <p>Hello, First Component</p>
    }
}

render(<FirstComponent />, document.getElementById('root'));
registerServiceWorker();

// TODO :- move component to a saprate folder 