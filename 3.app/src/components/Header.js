import React, { Component } from 'react';

class Header extends Component {
  render() {
    // console.log(this);
    return (
      <header className="head">
        <h1>Shopping list</h1>
        {/*<h3 className="list-name">list item</h3> */}
        <h3 className="list-name">{this.props.listItem}</h3>
      </header>
    );
  }
}
// TODO :- pass more props
export default Header;