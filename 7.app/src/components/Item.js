import React, { Component } from 'react';
import PropTypes from 'prop-types'; 

class Item extends Component {
  constructor(){
    super();
    this.itemClicke = this.itemClicke.bind(this);
  }
  itemClicke(event) {
    // console.log('button clicked', new Date().toString());
    // event.preventDefault();
    // console.log(this);
    const item = this.itemInput.value;
    this.context.router.history.push(`/first/${item}`)
  }
  render() {
    return (
      <div>
        <p>Item</p>
        <input type="text" ref={(input)=> {this.itemInput = input}} />
        <button onClick={this.itemClicke}>Click Me</button>
      </div>
    );
  }
}

Item.contextTypes = {
  router: PropTypes.object
}

export default Item;