import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './css/style.css';
import FirstComponent from './components/FirstComponent';
import NotFound from './components/NotFound';
import App from './components/App';

const Root = () => {
    return (
        <BrowserRouter>
            <div>
                <Switch>
                    <Route exact path='/' component={App} />
                    <Route path='/first' component={FirstComponent} />
                    <Route component={NotFound} />
                </Switch>
            </div>
        </BrowserRouter>
    )
}

render(<Root />, document.getElementById('root'));