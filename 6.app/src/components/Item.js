import React, { Component } from 'react';

class Item extends Component {
  constructor(){
    super();
    this.itemClicke = this.itemClicke.bind(this);
  }
  itemClicke(event) {
    // console.log('button clicked', new Date().toString());
    // event.preventDefault();
    console.log(this.itemInput.value);
  }
  
  // <button onClick={this.itemClicke.bind(this)}>Click Me</button>
  // <button onClick={(e) => this.itemClicke(e)}>Click Me</button>
  render() {
    return (
      <div>
        <p>Item</p>
        <input type="text" ref={(input)=> {this.itemInput = input}} />
        <button onClick={this.itemClicke}>Click Me</button>
      </div>
    );
  }
}

export default Item;