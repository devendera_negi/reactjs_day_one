import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import './css/style.css';
import FirstComponent from './components/FirstComponent';
import App from './components/App';

const Root = () => {
    return (
        <BrowserRouter>
            <div>
                <Route exact path='/' component={App} />
                <Route path='/first' component={FirstComponent} />
                <Router component={NotFound} />
            </div>
        </BrowserRouter>
    )
}

render(<Root />, document.getElementById('root'));
// TODO :- create a not found component