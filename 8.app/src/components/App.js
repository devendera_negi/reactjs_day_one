import React, { Component } from 'react';
import uuidv1 from 'uuid/v1';

import Header from './Header';
import Item from './Item';
import Cart from './Cart';

class App extends Component {
  constructor() {
    super();

    this.addItem = this.addItem.bind(this);
    // getinitialState
    this.state = {
      items: {},
      cart: {}
    }
  }
  addItem(item) {
    const items = {...this.state.items};
    const id = uuidv1();
    items[`item-${id}`] = item;
    this.setState({items});
  }
  render() {
    return (
      <div className="main-app">
        <div className="menu">
          <Header listItem="this is my list item" />
        </div>
        <Item addItem={this.addItem} />
        <Cart />
      </div>
    );
  }
}

export default App;
