import React, { Component } from 'react';
import uuidv1 from 'uuid/v1';

import Header from './Header';
import Item from './Item';
import Cart from './Cart';
import Stuff from './Stuff';

import defaultItems from '../defaultItems';

class App extends Component {
  constructor() {
    super();

    this.addItem = this.addItem.bind(this);
    this.loadDefault = this.loadDefault.bind(this);
    this.addToCart = this.addToCart.bind(this);
    // getinitialState
    this.state = {
      items: {},
      cart: {}
    }
  }
  addItem(item) {
    const items = {...this.state.items};
    const id = uuidv1();
    items[`item-${id}`] = item;
    this.setState({items});
  }

  loadDefault() {
    this.setState({
      items: defaultItems
    });
  }
  
  addToCart(key) {
    const cart = {...this.state.cart};

    cart[key] = cart[key] + 1 || 1;

    this.setState({ cart });

  }

  render() {
    return (
      <div className="main-app">
        <div className="menu">
          <Header listItem="this is my list item" />
            <ul className="list-of-items">
              {
                Object
                  .keys(this.state.items)
                  .map(key => <Stuff key={key} 
                    details={this.state.items[key]}
                    index={key}
                    addToCart={this.addToCart} />)

              }
            </ul>
        </div>
        <Item addItem={this.addItem} loadDefault={this.loadDefault} />
        <Cart items={this.state.items} cart={this.state.cart} />
      </div>
    );
  }
}

export default App;
